import { AngularStudyChatPage } from './app.po';

describe('angular-study-chat App', () => {
  let page: AngularStudyChatPage;

  beforeEach(() => {
    page = new AngularStudyChatPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
